var http = require('http');
var result;
function getJSON(options, cb){
	http.request(options, function(res){
	var body = '';

	res.on('data', function(chunk){
		body += chunk;
	});

	res.on('end',function(){
		 result = JSON.parse(body);
		cb(null,result);
	})
	res.on('error',cb);

})

	.on('error',cb)
	.end();
}

var options = {
	host: 'countryapi.gear.host',
	port: 80,
	path: '/v1/Country/getCountries?pName=greece',
	method:'GET'
};


getJSON(options, function(err,result){
	if(err){
		return console.log('Error while trying to get the country info',err);
	}else{
	console.log(result);
}

});

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'});
    res.write(JSON.stringify(result)); 
    res.end();
}).listen(process.env.PORT || 8080);
